package com.example.android.mvc_patterns;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer,View.OnClickListener {

    private Model mModel;
    private Button mButton1;
    private Button mButton2;
    private Button mButton3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mModel=new Model();
        mModel.addObserver(this);
        mButton1=findViewById(R.id.button1);
        mButton2=findViewById(R.id.button2);
        mButton3=findViewById(R.id.button3);

        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mButton3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.button1:
                mModel.setValueAtIndex(0);
                //Log.i("one","Button one is clicked");
                break;
            case R.id.button2:
                mModel.setValueAtIndex(1);

              //  Log.i("two","Button two is clicked");
                break;
            case R.id.button3:
                mModel.setValueAtIndex(2);

               // Log.i("three","Button three is clicked");
                break;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        mButton1.setText("Count : "+mModel.getValueAtIndex(0));
        mButton2.setText("Count : "+mModel.getValueAtIndex(1));
        mButton3.setText("Count : "+mModel.getValueAtIndex(2));

    }

}
